package main

import "gitee.com/ksbb/go-server/server"

func main() {
	tcpServer()
	//udpServer()
}

//tcp 服务器
func tcpServer() {
	server.NewServer("0.0.0.0", 8500)

	server.On("Connect", func() {
		//打印在控制台上
		server.ToConsole("Client: Connect.\n")
	})

	server.On("Send", func() {
		server.Send("server已经建立连接")
	})

	server.On("Close", func() {
		server.ToConsole("tcp 连接已断开")
	})

	server.Start()
}

//udp 服务器
func udpServer() {
	server.NewServer("0.0.0.0", 9800, "UDP")

	server.On("Connect", func() {
		//打印在控制台上
		server.ToConsole("udp Client: Connect.\n")
	})

	//server.On("Send", func() {
	//	server.SendUdp("你好啊，udp")
	//})

	server.On("Close", func() {
		server.ToConsole("udp 连接已断开")
	})

	server.Start()
}
