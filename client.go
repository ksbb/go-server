package main

import (
	"fmt"

	"gitee.com/ksbb/go-server/client"
)

func main() {
	tcpClient()
	//UdpClient()
}

//tcp 客户端
func tcpClient() {
	client.NewClient("0.0.0.0", 8500)

	client.On("Connect", func() {
		//打印在控制台上
		client.ToConsole("已连接服务器")
	})
	//发送给服务端
	client.On("Send", func() {
		client.Send("你好，udp server！！！")
	})

	client.On("Recv", func() {
		fmt.Println("接受到Udp服务器信息：====》")
		client.Recv()
	})
	client.On("Close", func() {
		client.ToConsole("连接已断开")
	})

	client.Start()
}

//tcp 客户端
func UdpClient() {
	client.NewClient("0.0.0.0", 9800, "UDP")

	client.On("Connect", func() {
		//打印在控制台上
		client.ToConsole("已连接udp服务器")
	})
	//发送给服务端
	client.On("Send", func() {
		client.Send("你好，Udp server！！！")
	})

	client.On("Recv", func() {
		fmt.Println("接受到Udp服务器信息：====》")
		client.Recv()
	})
	client.On("Close", func() {
		client.ToConsole("连接已断开")
	})

	client.Start()
}
