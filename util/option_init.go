package util

var (
	OptsT = make(map[string]func(), 0)
)

// 注册驱动事件
// opts ： 事件
// optst ：类型
// fopts ： 具体的方法
func RegisterOpts(opts string, fopts func()) {
	OptsT[opts] = fopts
}
