package server

import (
	"bufio"
	"fmt"
	"net"
	"strconv"

	"gitee.com/ksbb/go-server/util"
)

type tcpServer struct {
	Ip    string
	Port  int
	Conns []net.Conn
}

func newTcpServer(ip string, port int) *tcpServer {
	return &tcpServer{
		Ip:   ip,
		Port: port,
	}
}

//继承总类
func (t *tcpServer) Conn() {
	listen := t.Ip + ":" + strconv.Itoa(t.Port)
	server, _ := net.Listen("tcp", listen)
	if res := do("Connect"); res {
	} else {
		//调用回调函数
		fmt.Println("默认方法-监听 tcp 服务：0.0.0.1:8500")
	}

	for { //一直循环的监听连接
		conn, err := server.Accept() //建立与客户端的连接

		//fmt.Println("conn: ", conn)

		if err != nil {
			//建立连接的错误
			fmt.Println("err: ", err)
			continue
		}

		//逻辑的处理=>在用户发送完信息之后做什么处理
		go t.process(conn)
	}
}

func (t *tcpServer) process(conn net.Conn) { // Conn read .write
	//保存连接
	t.Conns = append(t.Conns, conn)
	defer t.Close(conn)
	for { //是为了循环输出客户端发过来的内容
		//借助缓冲来读取数据 ，数据的读取方式和文件是一样的，本身也是资源文件
		br := bufio.NewReader(conn)

		var buf [1024]byte
		n, err := br.Read(buf[:])

		if err != nil {
			fmt.Println("err ===> process: ", err)
			break
		}

		recStr := string(buf[:n])

		fmt.Println(recStr)
		//看有没有注册发送方法
		do("Send")
		//conn.Write([]byte(`hello world im is tcp server`))
	}
}

func (t *tcpServer) Close(conn net.Conn) {
	conn.Close()
	do("Close")
}

func do(e string) bool {
	opt, ok := util.OptsT[e] //看有没有注册发送方法
	if ok {
		opt()
		return true
	}
	return false
}

func (t *tcpServer) Send(c net.Conn, s string) {
	c.Write([]byte(s))
}
