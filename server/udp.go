package server

import (
	"fmt"
	"net"
)

type udpServer struct {
	Ip    string
	Port  int
	Conns *net.UDPConn
	Add   *net.UDPAddr
}

func newUdpServer(ip string, port int) *udpServer {
	//ip需要分割
	return &udpServer{
		Ip:   ip,
		Port: port,
	}
}

//继承总类
func (t *udpServer) Conn() {
	conn, _ := net.ListenUDP("udp", &net.UDPAddr{
		IP:   net.IPv4(0, 0, 0, 0),
		Port: t.Port,
	})
	fmt.Printf("%T:--", conn)
	t.Conns = conn
	defer t.Close(t.Conns)
	for {
		var data [1024]byte
		n, addr, _ := conn.ReadFromUDP(data[:])
		t.Add = addr
		fmt.Println(n, addr, string(data[:n]))
		do("Send")
	}
}

func (t *udpServer) Send(conn *net.UDPConn, addr *net.UDPAddr, s string) {
	conn.WriteToUDP([]byte(s), addr)
}

func (t *udpServer) Recv(conn *net.UDPConn) {
	var data [1024]byte
	n, addr, _ := conn.ReadFromUDP(data[:])
	fmt.Println(n, addr, string(data[:n]))
}

func (t *udpServer) Close(conn *net.UDPConn) {
	conn.Close()
	do("Close")
}
