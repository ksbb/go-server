package server

import (
	"fmt"
	"net"
	"reflect"

	"gitee.com/ksbb/go-server/util"
)

type Handler func()

type Register struct {
}
type Server interface {
	Conn()
}

type Opt map[string]Handler

var s Server
var defaultServer Server

func NewServer(ip string, port int, sType ...string) {
	if len(sType) != 0 && sType[0] == "UDP" {
		defaultServer = newUdpServer(ip, port)
	}

	defaultServer = newTcpServer(ip, port)
	fmt.Println(defaultServer)
}

//默认的服务器
func GetServer() Server {
	return defaultServer
}

//开始服务器
func Start() {
	//反射调用
	v := reflect.ValueOf(defaultServer)
	v.MethodByName("Conn").Call(nil)
}

func On(event string, f Handler) {
	s = GetServer()
	util.RegisterOpts(event, f)
}

//发送到用户台
func ToConsole(str string) {
	fmt.Println(str)
}

//发送到客户端
func Send(str string) {
	v := reflect.ValueOf(defaultServer)
	conn := v.Elem().FieldByName("Conns").Interface().([]net.Conn)
	//fmt.Println(conn)
	////获取最新那条连接
	c := conn[len(conn)-1]
	v.MethodByName("Send").Call([]reflect.Value{
		reflect.ValueOf(c),
		reflect.ValueOf(str),
	})

}

func SendUdp(str string) {
	fmt.Println(defaultServer)
	v := reflect.ValueOf(defaultServer)
	//t := v.Elem().FieldByName("Conns").Type().String()
	//t2 := v.Elem().FieldByName("Add").Type().String()
	//fmt.Println(v)
	//fmt.Println(t, t2)
	//conn := v.Elem().FieldByName("Conns").Interface().(*net.UDPConn)
	conn := v.Elem().FieldByName("Conns").Interface().([]net.Conn)
	addr := v.Elem().FieldByName("Add").Interface().(*net.UDPAddr)
	//fmt.Println(conn)
	v.MethodByName("Send").Call([]reflect.Value{
		reflect.ValueOf(conn),
		reflect.ValueOf(addr),
		reflect.ValueOf(str),
	})

}
