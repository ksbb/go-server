package client

import (
	"fmt"
	"net"
)

type udpClient struct {
	Ip    string
	Port  int
	Conns *net.UDPConn
	Adrr  *net.UDPAddr
}

func newUdpClient(ip string, port int) *tcpClient {
	return &tcpClient{
		Ip:   ip,
		Port: port,
	}
}

//连接
func (t *udpClient) Conn() {
	c, _ := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   net.IPv4(127, 0, 0, 1),
		Port: 9800,
	})
	t.Conns = c
}

func (t *udpClient) Send(socket net.UDPConn, s string) {
	socket.WriteToUDP([]byte(s), t.Adrr)
}

func (t *udpClient) Recv(socket net.UDPConn) {
	var data [1024]byte
	n, addr, _ := socket.ReadFromUDP(data[:])
	t.Adrr = addr
	fmt.Println(n, addr, string(data[:n]))
}
